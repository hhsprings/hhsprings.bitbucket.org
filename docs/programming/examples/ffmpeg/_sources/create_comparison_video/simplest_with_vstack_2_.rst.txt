simplest with vstack (2)
########################

.. my_youtube:: YRCc6NhSskA

:doc: https://ffmpeg.org/ffmpeg-filters.html#vstack,
      https://ffmpeg.org/ffmpeg-filters.html#amerge,
      https://ffmpeg.org/ffmpeg-filters.html#pan

This is also a simple approach, maintaining the aspect ratio. Just personally I do not like this because each video size is too small.

.. code-block:: bash

    #! /bin/sh
    pref="`basename $0 .sh`"
    vleft="EuropeanArchive.mp4"
    vright="Eduardo.mp4"
    
    #
    ffmpeg -y -i "${vleft}" -i "${vright}" -filter_complex "
    [0:v]scale=960:540,pad=1920:540:(ow-iw)/2:0,setsar=1[0v];
    [1:v]scale=960:540,pad=1920:540:(ow-iw)/2:0,setsar=1[1v];
    [0v][1v]vstack[v];
    
    [0:a][1:a]amerge=inputs=2,pan=stereo|c0<c0+c1|c1<c2+c3[a]
    " -map '[v]' -map '[a]' \
      "${pref}.mp4"
