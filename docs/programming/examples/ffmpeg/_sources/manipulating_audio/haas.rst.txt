.. _haas:

haas
####
.. my_youtube:: QEJwIdpZ8w8

:doc: https://ffmpeg.org/ffmpeg-filters.html#haas

This filter applies a `Haas effect (Precedence effect) <https://en.wikipedia.org/wiki/Haas_effect>`__ to the audio. This filter is not available on ffmpeg 3.x.

The description in the official documentation:

    Apply Haas effect to audio.
    
    Note that this makes most sense to apply on mono signals.
    With this filter applied to mono signals it give some directionality and stretches its stereo image.
    
    The filter accepts the following options:
    
    * level_in

      Set input level. By default is 1, or 0dB
    
    * level_out

      Set output level. By default is 1, or 0dB.
    
    * side_gain

      Set gain applied to side part of signal. By default is 1.
    
    * middle_source

      Set kind of middle source. Can be one of the following:
    
      * left

        Pick left channel.
    
      * right

        Pick right channel.
    
      * mid

        Pick middle part signal of stereo image. (default)
    
      * side

        Pick side part signal of stereo image.
    
    * middle_phase

      Change middle phase. By default is disabled.
    
    * left_delay

      Set left channel delay. By default is 2.05 milliseconds.
    
    * left_balance

      Set left channel balance. By default is -1.
    
    * left_gain

      Set left channel gain. By default is 1.
    
    * left_phase

      Change left phase. By default is disabled.
    
    * right_delay

      Set right channel delay. By defaults is 2.12 milliseconds.
    
    * right_balance

      Set right channel balance. By default is 1.
    
    * right_gain

      Set right channel gain. By default is 1.
    
    * right_phase

      Change right phase. By default is enabled.

For the purpose of "Precedence effect", it is meaningless to give :code:`delay` a zero. But to understand the control of :code:`phase` and :code:`balance`, it is better to start by giving :code:`delay` zero:

.. code-block:: console

    [me@host: ~]$ ffmpeg411="/c/Program Files/ffmpeg-4.1.1-win64-shared/bin/ffmpeg"
    [me@host: ~]$ "${ffmpeg411}" -i input.wav -af "
    > haas=
    >  left_phase=0:right_phase=1
    > :left_balance=-0.75:right_balance=1
    > :left_delay=10:right_delay=0" -f wav - | (ffplay - 2>/dev/null)

Each segment of the uploaded video was created with the following script:

.. code-block:: bash
    :emphasize-lines: 6,7,8,9,10,11,12,13,14,15

    #! /bin/sh
    ffmpeg411="/c/Program Files/ffmpeg-4.1.1-win64-shared/bin/ffmpeg"
    #
    "${ffmpeg411}" -y -filter_complex "
    amovie=input.wav
    ,haas=
     left_phase=0:right_phase=1
    :left_balance=-1:right_balance=1
    :left_delay=40:right_delay=5
    
    :left_gain=1:right_gain=1
    :level_in=1:level_out=1
    :middle_phase=0
    :middle_source=mid
    :side_gain=1
    
    ,asplit=3[out1][a0],channelsplit[a1][a2];
    [a1]showcqt=s=1440x360,crop=1280:360:0,setsar=1[v1];
    [a2]showcqt=s=1440x360,crop=1280:360:0,setsar=1,vflip[v2];
    [a0]showwaves=s=426x720:split_channels=1:mode=cline,setsar=1[v3];
    [v1][v2]vstack,scale=852:720,setsar=1[v4];
    [v4][v3]hstack,scale=1280:720,setsar=1[out0]
    " -map '[out0]' -map '[out1]' output.mp4

By the way, this filter can be used as one of the "monoral to stereo tool":

.. code-block:: console
    :class: longwidth-code

    [me@host: ~]$ ffprobe -hide_banner quotes_E.wav
    Input #0, wav, from 'quotes_E.wav':
      Metadata:
        encoder         : Lavf57.71.100
      Duration: 00:00:37.15, bitrate: 352 kb/s
        Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), 22050 Hz, 1 channels (FL), s16, 352 kb/s
    [me@host: ~]$ #
    [me@host: ~]$ ffmpeg411="/c/Program Files/ffmpeg-4.1.1-win64-shared/bin/ffmpeg"
    [me@host: ~]$ "${ffmpeg411}" -i quotes_E.wav -af haas quotes_E_haas.wav
      ...
    [me@host: ~]$ ffprobe -hide_banner quotes_E_haas.wav
    Input #0, wav, from 'quotes_E_haas.wav':
      Metadata:
        encoder         : Lavf58.20.100
      Duration: 00:00:37.15, bitrate: 705 kb/s
        Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), 22050 Hz, 2 channels, s16, 705 kb/s
