.. _channelmap:

channelmap
##########
:doc: https://ffmpeg.org/ffmpeg-filters.html#channelmap,
      :ref:`utildoc_channellayout`

.. my_spacer:: 0.3em

:see also: :ref:`pan`, :ref:`channelsplit_join_amix_amerge`

If you need gain control as well as channel remapping,
what you want is probably a :ref:`pan` filter.
If you simply want to do channel remapping instead,
this filter is easier to use than the :ref:`pan` filter.

However, if you already know the :ref:`pan` filter and use this
for the first time, you may be a little confused. For example:

.. code-block:: console

    [me@host: ~]$ ffmpeg -i in.mov -filter 'channelmap=map=DL-FL|DR-FR' out.wav

:code:`DL - FL` **does not mean** "*subtract channel FL value from channel DL value*",
rather, this means "*let the input be DL and output it to FL*".
So, the above example behaves as "using downmix as 2ch stereo"
if the input is a 5.1+downmix.

Note also the form of "channel number" is different from of :ref:`pan`:

.. code-block:: console

    [me@host: ~]$ # swapping LR
    [me@host: ~]$ ffmpeg -y -i 2chstereo.wav -filter_complex "
    > [0:a]pan='stereo|c0=c1|c1=c0'" out.wav

.. code-block:: console

    [me@host: ~]$ # swapping LR
    [me@host: ~]$ ffmpeg -y -i 2chstereo.wav -filter_complex "
    > [0:a]channelmap='map=1-0|0-1':channel_layout=stereo" out.wav
    [me@host: ~]$ # If out_channel is not given then it is
    [me@host: ~]$ # implicitly an index, starting with zero and
    [me@host: ~]$ # increasing by one for each mapping.
    [me@host: ~]$ ffmpeg -y -i 2chstereo.wav -filter_complex "
    > [0:a]channelmap='map=1|0':channel_layout=stereo" out.wav
