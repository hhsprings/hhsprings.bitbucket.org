.. _aphaser:

aphaser
#######
.. my_youtube::  c2yrKerFIkg

:doc: https://ffmpeg.org/ffmpeg-filters.html#aphaser

The description in the official documentation:

    Add `a phasing effect <https://en.wikipedia.org/wiki/Phaser_(effect)>`__ to the input audio.
    
    A phaser filter creates series of peaks and troughs in the frequency spectrum. The position of the peaks and troughs are modulated so that they vary over time, creating a sweeping effect.

Each segment of the uploaded video was created with the following script:

.. code-block:: bash
    :emphasize-lines: 6,7,8,9,10,11,12

    #! /bin/sh
    ffmpeg -hide_banner -y -filter_complex "
    amovie=wolframtones_01.wav
    ,atrim=0:15,afade=t=out:st=14.5:d=0.5

    ,aphaser=
     in_gain=0.4
    :out_gain=0.74
    :delay=3.0
    :decay=0.4
    :speed=0.5
    :type=t

    ,aformat=sample_fmts=u8|s16:channel_layouts=stereo
    ,asplit=3[out1][a1][a2];
    [a1]showfreqs=s=$((1280/5*3))x720:fscale=log:cmode=separate[v1];
    [a2]showwaves=s=$((1280/5*2))x720:mode=cline:split_channels=1[v2];
    [v2][v1]hstack
    ,format=yuv420p
    [out0]" -map '[out0]' -map '[out1]' out.mp4

The effect of this is so obvious that I won't say anything.

:see also: :ref:`flanger`
