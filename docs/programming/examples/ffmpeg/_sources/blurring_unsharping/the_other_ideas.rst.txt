.. _blur_other_ideas:

the other ideas
###############

.. my_youtube:: Cuf1ImDtDqQ

:doc: https://ffmpeg.org/ffmpeg-filters.html#noise,
      https://ffmpeg.org/ffmpeg-filters.html#geq,
      https://ffmpeg.org/ffmpeg-filters.html#blend_002c-tblend

If the purpose of your "blur" is to make it difficult to identify the object and if you want an interesting visual effect, you may feel that you are unsatisfactory only by what we have introduced so far. This is not that simple, but I will introduce only two ideas.

with \`noise'
*************

.. my_youtubelink:: Cuf1ImDtDqQ&t=25

The first method is to use "noise". As you can see from the results shown after this, the effect is not very good on its own but it may be better to consider combinations with something else.

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_2903.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    #
    cx=220 ; cy=300
    x=880 ; y=300
    #
    strength=${strength:-50}
    flags="${flags:-'t+a'}"  # "t+a", "t+u", "t+p", etc.
    #
    ffmpeg -y -i "${ifn}" -filter_complex "
    [0:v]split[0v1][0v2];
    
    [0v1]crop=${cx}:${cy}:${x}:${y},setsar=1,
    noise=alls=${strength}:allf=${flags}
    
    ,drawbox=c=blue[b];
    
    [0v2][b]overlay=${x}:${y}[v]
    " -map '[v]' -an "${pref}_${ifnb}.mp4"

with \`geq' and \`blend'
************************

.. my_youtubelink:: Cuf1ImDtDqQ&t=75

The other uses "geq" and "blend" to give a checkerboard effect.

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_2903.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    #
    cx=220 ; cy=300
    x=880 ; y=300
    #
    gridsize=${gridsize:-32}
    #
    ffmpeg -y -i "${ifn}" -filter_complex "
    [0:v]crop=${cx}:${cy}:${x}:${y},setsar=1,split=2[0v1][0v2];
    
    [0v1]
    geq='
    lum=lum(X,Y)*(1+(1.5-(mod(floor(T*2),2)+1))/10):
    cr=cr(X,Y):cb=cb(X,Y)'
    [d1];
    [0v2]
    geq='
    lum=lum(X,Y)*(1-(1.5-(mod(floor(T*2),2)+1))/10):
    cr=cr(X,Y):cb=cb(X,Y)'
    [d2];
    
    [d1][d2]
    blend=all_expr='
    if(eq(mod(floor(X/${gridsize}),2),mod(floor(Y/${gridsize}),2)),A,B)
    '
    ,drawbox=c=blue[b];
    
    [0:v][b]overlay=${x}:${y}[v]
    " -map '[v]' -an "${pref}_${ifnb}.mp4"
