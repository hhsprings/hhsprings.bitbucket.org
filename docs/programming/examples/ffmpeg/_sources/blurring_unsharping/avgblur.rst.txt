avgblur
#######

.. my_youtube:: -jhHrrEIGyo

:doc: https://ffmpeg.org/ffmpeg-filters.html#avgblur

"avgblur" is probably the simplest of several "blurring" methods in ffmpeg. It is probably just taking the average with the octagonal pixels around. Because of this simplicity, it is probably best if you want to "destroy images and make them unidentifiable", close to so-called mosaic processing. Conversely speaking, it is not suitable for "smoothing beautifully".

* sizeX: horizontal radius size.
* planes: which planes to filter. By default all planes are filtered.
* sizeY vertical radius size, if zero it will be same as sizeX. Default is 0.

.. code-block:: bash

    #! /bin/sh
    ifn="Grape_Vineyard.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    #
    radius=${radius:-10}
    #
    ffmpeg -y -i "${ifn}" -filter_complex "
    [0:v]split[0v1][0v2];
    
    [0v1]crop=900:500:30:580,setsar=1,
    avgblur=sizeX=${radius}
    
    ,drawbox=c=blue[b];
    
    [0v2][b]overlay=30:580[v]
    " -map '[v]' -an "${pref}_${ifnb}.mp4"
