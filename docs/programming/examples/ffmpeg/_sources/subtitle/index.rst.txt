.. role:: namespace(literal)

subtitle
########
You can draw subtitles on top of input video by using `subtitles <https://ffmpeg.org/ffmpeg-filters.html#subtitles-1>`_ filter. Note that this is so-called `Hardsub <https://en.wikipedia.org/w/index.php?title=Hardsub&redirect=no>`_, that is, you can not turn off the subtitles.

You can use two types of subtitles in ffmpeg, `SubRip <https://en.wikipedia.org/wiki/SubRip>`_ and `(Advanced) SubStation Alpha <https://en.wikipedia.org/wiki/SubStation_Alpha>`_.

* `SubRip <https://en.wikipedia.org/wiki/SubRip>`_ is restrictive but very simple and easy to use.
* "(Advanced) SubStation Alpha" is more complex than SubRip, but it can do more than SubRip.

  * The complete specification for "(Advanced) SubStation Alpha" can be obtained from `ass-specs.doc <http://moodub.free.fr/video/ass-specs.doc>`_.

Table of Contents
*****************

.. toctree::
    :maxdepth: 3

    srt
    ass
    ass_tips
