.. _showspatial:

showspatial
###########
.. my_youtube::  mcWeZ7Ma5Qo

:doc: https://ffmpeg.org/ffmpeg-filters.html#showspatial

This filter is not available on ffmpeg 4.1.

The description in the official documentation:

    Convert stereo input audio to a video output,
    representing the *spatial relationship between two channels*.

The commandline in uploaded video was:

.. code-block:: bash

    #! /bin/sh
    ffmpeg421="/c/Program Files/ffmpeg-4.2.1-win64-shared/bin/ffmpeg"
    for f in "" ",aphaser" ",apulsator" ; do
        for i in *.mp4 *.wav ; do (
            set -x ; "${ffmpeg421}" -filter_complex "
            amovie=$i,atrim=0:4
            ${f}
            ,asplit=3[out1][a1][a2];
            [a1]showfreqs=s=$((16*60/5*2))x$((9*60)):fscale=log:cmode=separate[v1];
            [a2]showspatial=s=$((16*60/5*3))x$((9*60))[v2];
            [v2][v1]hstack,format=yuv420p[out0]" \
            -map '[out0]' -map '[out1]' -f matroska - 2>/dev/null) | (
            ffplay - -autoexit 2>/dev/null)
        done
    done
