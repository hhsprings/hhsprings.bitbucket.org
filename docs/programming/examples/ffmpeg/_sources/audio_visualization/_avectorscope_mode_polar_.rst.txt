\`avectorscope=mode=polar'
##########################

.. my_youtube:: FnZwZwuuAGY

:doc: https://ffmpeg.org/ffmpeg-filters.html#avectorscope

.. code-block:: bash

    #! /bin/sh
    ifn="Air on the G String (from Orchestral Suite no. 3, BWV 1068).mp3"
    ifnb="`basename \"${ifn}\" .mp3`"
    pref="`basename $0 .sh`"
    
    #
    ffmpeg -y -i "${ifn}" -filter_complex "
    [0:a]avectorscope=mode=polar:s=1920x1080[v]
    " -map '[v]' -map '0:a' -c:a copy \
      "${pref}_${ifnb}.mp4"
