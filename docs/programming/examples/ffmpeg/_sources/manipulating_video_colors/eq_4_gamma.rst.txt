eq (4) - gamma
##############

.. my_youtube:: 4Ko38xnvN7s

:doc: https://ffmpeg.org/ffmpeg-filters.html#eq

gamma=0.1 ... 10.0
******************
The value must be a float value in range 0.1 to 10.0. The default value is "1".

**eq=gamma=0.1**

.. my_youtubelink:: 4Ko38xnvN7s&t=0

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers_fast.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=0.1" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=0.5**

.. my_youtubelink:: 4Ko38xnvN7s&t=8

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers_fast.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=0.5" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=1.0**

.. my_youtubelink:: 4Ko38xnvN7s&t=15

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers_fast.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=1.0" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=5.0**

.. my_youtubelink:: 4Ko38xnvN7s&t=23

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers_fast.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=5.0" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=10.0**

.. my_youtubelink:: 4Ko38xnvN7s&t=31

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers_fast.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=10.0" \
      -an "${pref}_${ifnb}.mp4"

Using expression
****************
In these examples, since "${ifn}" is  a video of 30 seconds, it instructs to change  by approximately 1 / 3 per second.

gamma_weight can be used to reduce the effect of  a high gamma value on bright image areas, e.g. keep  them from getting overamplified and just plain white.  The value must be a float in range 0.0 to 1.0. A value  of 0.0 turns the gamma correction all the way down  while 1.0 leaves it at its full strength. Default is "1".

**eq=gamma=(0.1+t/30.4\*10.0):eval=frame:gamma_weight=1.0**

.. my_youtubelink:: 4Ko38xnvN7s&t=38

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=(0.1+t/30.4*10.0):eval=frame:gamma_weight=1.0" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=(0.1+t/30.4\*10.0):eval=frame:gamma_weight=0.5**

.. my_youtubelink:: 4Ko38xnvN7s&t=68

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=(0.1+t/30.4*10.0):eval=frame:gamma_weight=0.5" \
      -an "${pref}_${ifnb}.mp4"

**eq=gamma=(0.1+t/30.4\*10.0):eval=frame:gamma_weight=0.0**

.. my_youtubelink:: 4Ko38xnvN7s&t=98

.. code-block:: bash

    #! /bin/sh
    ifn="Pexels_flowers.mp4"
    ifnb="`basename \"${ifn}\" .mp4`"
    pref="`basename $0 .sh`"
    
    ffmpeg -y -i "${ifn}" \
      -vf "eq=gamma=(0.1+t/30.4*10.0):eval=frame:gamma_weight=0.0" \
      -an "${pref}_${ifnb}.mp4"

:see also: :ref:`expression_evaluation`
