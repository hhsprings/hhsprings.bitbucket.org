ImageOps Module
###############

.. note::

  All source images in this document are derived from https://www.pexels.com (CC0 License).

.. |srcimg01| image:: _static/exams/data/srcimg01.jpg
  :width: 20%

.. |srcimg02| image:: _static/exams/data/srcimg02.jpg
  :width: 20%

.. |srcimg03| image:: _static/exams/data/srcimg03.jpg
  :width: 20%

.. |srcimg04| image:: _static/exams/data/srcimg04.jpg
  :width: 20%

.. |srcimg05| image:: _static/exams/data/srcimg05.jpg
  :width: 20%

.. |srcimg06| image:: _static/exams/data/srcimg06.jpg
  :width: 20%

.. |srcimg07| image:: _static/exams/data/srcimg07.jpg
  :width: 20%

.. |srcimg11| image:: _static/exams/data/srcimg11.png
  :width: 20%

.. |srcimg12| image:: _static/exams/data/srcimg12.jpg
  :width: 20%

.. |srcimg13| image:: _static/exams/data/srcimg13.jpg
  :width: 20%

.. |srcimg14| image:: _static/exams/data/srcimg14.jpg
  :width: 20%

.. |srcimg17| image:: _static/exams/data/srcimg17.jpg
  :width: 20%

.. |srcimg18| image:: _static/exams/data/srcimg18.jpg
  :width: 20%

.. |srcimg19| image:: _static/exams/data/srcimg19.jpg
  :width: 20%

.. |srcimg20| image:: _static/exams/data/srcimg20.jpg
  :width: 20%

.. |srcimg21| image:: _static/exams/data/srcimg21.jpg
  :width: 20%

.. |srcimg22| image:: _static/exams/data/srcimg22.jpg
  :width: 20%

.. |srcimg23| image:: _static/exams/data/srcimg23.jpg
  :width: 20%

Functions
*********

autocontrast
============
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.autocontrast,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.autocontrast

.. literalinclude:: _static/exams/ImageOps_autocontrast_01.py

.. |ImageOps_autocontrast.res201| image:: _static/exams/result/ImageOps_autocontrast_srcimg20_01.jpg
.. |ImageOps_autocontrast.res202| image:: _static/exams/result/ImageOps_autocontrast_srcimg20_02.jpg
.. |ImageOps_autocontrast.res203| image:: _static/exams/result/ImageOps_autocontrast_srcimg20_03.jpg
.. |ImageOps_autocontrast.res211| image:: _static/exams/result/ImageOps_autocontrast_srcimg21_01.jpg
.. |ImageOps_autocontrast.res212| image:: _static/exams/result/ImageOps_autocontrast_srcimg21_02.jpg
.. |ImageOps_autocontrast.res213| image:: _static/exams/result/ImageOps_autocontrast_srcimg21_03.jpg
.. |ImageOps_autocontrast.res221| image:: _static/exams/result/ImageOps_autocontrast_srcimg22_01.jpg
.. |ImageOps_autocontrast.res222| image:: _static/exams/result/ImageOps_autocontrast_srcimg22_02.jpg
.. |ImageOps_autocontrast.res223| image:: _static/exams/result/ImageOps_autocontrast_srcimg22_03.jpg
.. |ImageOps_autocontrast.res231| image:: _static/exams/result/ImageOps_autocontrast_srcimg23_01.jpg
.. |ImageOps_autocontrast.res232| image:: _static/exams/result/ImageOps_autocontrast_srcimg23_02.jpg
.. |ImageOps_autocontrast.res233| image:: _static/exams/result/ImageOps_autocontrast_srcimg23_03.jpg


.. |srcimg20_100| image:: _static/exams/data/srcimg20.jpg
.. |srcimg21_100| image:: _static/exams/data/srcimg21.jpg
.. |srcimg22_100| image:: _static/exams/data/srcimg22.jpg
.. |srcimg23_100| image:: _static/exams/data/srcimg23.jpg

.. list-table::

  * - src |srcimg20_100|
    - cutoff=0 |ImageOps_autocontrast.res201|
    - cutoff=0.2 |ImageOps_autocontrast.res202|
    - cutoff=0.4 |ImageOps_autocontrast.res203|
  * - src |srcimg21_100|
    - cutoff=0 |ImageOps_autocontrast.res211|
    - cutoff=0.2 |ImageOps_autocontrast.res212|
    - cutoff=0.4 |ImageOps_autocontrast.res213|
  * - src |srcimg22_100|
    - cutoff=0 |ImageOps_autocontrast.res221|
    - cutoff=0.2 |ImageOps_autocontrast.res222|
    - cutoff=0.4 |ImageOps_autocontrast.res223|
  * - src |srcimg23_100|
    - cutoff=0 |ImageOps_autocontrast.res231|
    - cutoff=0.2 |ImageOps_autocontrast.res232|
    - cutoff=0.4 |ImageOps_autocontrast.res233|

colorize
========
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.colorize,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.colorize

.. list-table::

  * - srcimg23.jpg |srcimg23|

.. |ImageOps.colorize.res01| image:: _static/exams/result/ImageOps_colorize_01.jpg

.. list-table::
  :widths: 20 10

  * - .. literalinclude:: _static/exams/ImageOps_colorize_01.py
    - |ImageOps.colorize.res01|

crop
====
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.crop,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.crop

.. list-table::

  * - srcimg11.png |srcimg11|

.. |ImageOps.crop.res01| image:: _static/exams/result/ImageOps_crop_01.jpg

.. list-table::
  :widths: 20 10

  * - .. literalinclude:: _static/exams/ImageOps_crop_01.py
    - |ImageOps.crop.res01|

deform
======
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.deform,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.deform

Deform the image using the given deformer object. The deformer should provide a getmesh method, which returns a MESH list suitable for the Image `transform <Image__class_Image.html#transform-size-mesh-data>`_ method. See the `transform <Image__class_Image.html#transform-size-mesh-data>`_ method for details.

.. list-table::

  * - srcimg17.jpg |srcimg17|

.. |ImageOps_deform.res1| image:: _static/exams/result/ImageOps_deform_01.jpg

.. list-table::

  * - .. literalinclude:: _static/exams/ImageOps_deform_01.py
    - |ImageOps_deform.res1|

equalize
========
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.equalize,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.equalize

.. list-table::

  * - srcimg07.jpg |srcimg07|

.. |ImageOps_equalize.res1| image:: _static/exams/result/ImageOps_equalize_sgs.jpg
.. |ImageOps_equalize.res2| image:: _static/exams/result/ImageOps_equalize_d.jpg
.. |ImageOps_equalize.res3| image:: _static/exams/result/ImageOps_equalize_dgs.jpg
.. |ImageOps_equalize.res4| image:: _static/exams/result/ImageOps_equalize_hist_01.jpg

.. literalinclude:: _static/exams/ImageOps_equalize_01.py

.. list-table::

  * - convert("L") to orig |ImageOps_equalize.res1|
    - equalize to orig |ImageOps_equalize.res2|
    - convert("L") to equalized |ImageOps_equalize.res3|
    - histogram |ImageOps_equalize.res4|

expand
======
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.expand,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.expand

.. list-table::

  * - srcimg05.jpg |srcimg05|

.. |ImageOps.expand.res01| image:: _static/exams/result/ImageOps_expand_01.jpg
.. |ImageOps.expand.res02| image:: _static/exams/result/ImageOps_expand_02.jpg
.. |ImageOps.expand.res03| image:: _static/exams/result/ImageOps_expand_03.jpg
.. |ImageOps.expand.res04| image:: _static/exams/result/ImageOps_expand_04.jpg
.. |ImageOps.expand.res05| image:: _static/exams/result/ImageOps_expand_05.jpg
.. |ImageOps.expand.res06| image:: _static/exams/result/ImageOps_expand_06.jpg

.. list-table::
  :widths: 15 10 10

  * - .. literalinclude:: _static/exams/ImageOps_expand_01.py
    - border=100, fill=0 |ImageOps.expand.res01|
    - border=100, fill="burlywood" |ImageOps.expand.res02|

  * - .. literalinclude:: _static/exams/ImageOps_expand_02.py
    - border=(100, 50) |ImageOps.expand.res03|
    - border=(20, 30, 40, 50) |ImageOps.expand.res04|

  * - .. literalinclude:: _static/exams/ImageOps_expand_03.py
    - |ImageOps.expand.res05|
    - |ImageOps.expand.res06|

fit
===
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.fit,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.fit

.. list-table::

  * - srcimg11.png |srcimg11|

.. |ImageOps.fit.res01| image:: _static/exams/result/ImageOps_fit_01.jpg
  :width: 40%
.. |ImageOps.fit.res02| image:: _static/exams/result/ImageOps_fit_02.jpg
  :width: 40%
.. |ImageOps.fit.res03| image:: _static/exams/result/ImageOps_fit_03.jpg
  :width: 40%
.. |ImageOps.fit.res04| image:: _static/exams/result/ImageOps_fit_04.jpg
  :width: 15%
.. |ImageOps.fit.res05| image:: _static/exams/result/ImageOps_fit_05.jpg
  :width: 15%
.. |ImageOps.fit.res06| image:: _static/exams/result/ImageOps_fit_06.jpg
  :width: 15%

.. list-table::
    :widths: 45 55

    * - .. literalinclude:: _static/exams/ImageOps_fit_01.py
      - | |ImageOps.fit.res01| height=1/3, centering=0
        | |ImageOps.fit.res04| width=1/3, centering=0
        | |ImageOps.fit.res02| height=1/3, centering=0.5
        | |ImageOps.fit.res05| width=1/3, centering=0.5
        | |ImageOps.fit.res03| height=1/3, centering=1
        | |ImageOps.fit.res06| width=1/3, centering=1

flip
====
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.flip,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.flip

:code:`PIL.ImageOps.flip(image)` is identical to :code:`image.transpose(PIL.Image.FLIP_TOP_BOTTOM)`.

grayscale
=========
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.grayscale,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.grayscale

:code:`PIL.ImageOps.grayscale(image)` is identical to :code:`image.convert("L")`.

invert
======
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.invert,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.invert

.. list-table::

  * - srcimg18.jpg |srcimg18|

.. |ImageOps.invert.res01| image:: _static/exams/result/ImageOps_invert_01.jpg

.. list-table::
  :widths: 20 10

  * - .. literalinclude:: _static/exams/ImageOps_invert_01.py
    - |ImageOps.invert.res01|

mirror
======
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.mirror,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.mirror

:code:`PIL.ImageOps.mirror(image)` is identical to :code:`image.transpose(PIL.Image.FLIP_LEFT_RIGHT)`.

posterize
=========
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.posterize,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.posterize

.. list-table::

  * - srcimg06.jpg |srcimg06|

.. |ImageOps.posterize.res01| image:: _static/exams/result/ImageOps_posterize_b1.jpg
.. |ImageOps.posterize.res02| image:: _static/exams/result/ImageOps_posterize_b2.jpg
.. |ImageOps.posterize.res03| image:: _static/exams/result/ImageOps_posterize_b3.jpg
.. |ImageOps.posterize.res04| image:: _static/exams/result/ImageOps_posterize_b4.jpg
.. |ImageOps.posterize.res05| image:: _static/exams/result/ImageOps_posterize_b5.jpg
.. |ImageOps.posterize.res06| image:: _static/exams/result/ImageOps_posterize_b6.jpg
.. |ImageOps.posterize.res07| image:: _static/exams/result/ImageOps_posterize_b7.jpg
.. |ImageOps.posterize.res08| image:: _static/exams/result/ImageOps_posterize_b8.jpg

.. literalinclude:: _static/exams/ImageOps_posterize_01.py

.. list-table::

      * - bits=1 |ImageOps.posterize.res01|
        - bits=2 |ImageOps.posterize.res02|
        - bits=3 |ImageOps.posterize.res03|
        - bits=4 |ImageOps.posterize.res04|
      * - bits=5 |ImageOps.posterize.res05|
        - bits=6 |ImageOps.posterize.res06|
        - bits=7 |ImageOps.posterize.res07|
        - bits=8 |ImageOps.posterize.res08|

solarize
========
:doc: https://pillow.readthedocs.io/en/latest/reference/ImageOps.html#PIL.ImageOps.solarize,
      http://effbot.org/imagingbook/imageops.htm#tag-ImageOps.solarize

.. list-table::

  * - srcimg12.jpg |srcimg12|

.. |ImageOps.solarize.res01| image:: _static/exams/result/ImageOps_solarize_01.jpg
.. |ImageOps.solarize.res02| image:: _static/exams/result/ImageOps_solarize_02.jpg
.. |ImageOps.solarize.res03| image:: _static/exams/result/ImageOps_solarize_03.jpg

.. literalinclude:: _static/exams/ImageOps_solarize_01.py

.. list-table::

      * - threshold=128 |ImageOps.solarize.res01|
        - threshold=50 |ImageOps.solarize.res02|
        - threshold=0 |ImageOps.solarize.res03|
