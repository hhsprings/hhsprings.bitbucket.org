import math
from PIL import ImagePath

def _create_demo_imgs(xy, size):
    # to clarify behaviour of compact method,
    # this demo shows up 'line' version and
    # 'point' version.
    from PIL import Image, ImageDraw
    imgp = Image.new("L", size, color=240)
    dctx = ImageDraw.Draw(imgp)
    dctx.point(xy)
    del dctx
    imgl = Image.new("L", size, color=240)
    dctx = ImageDraw.Draw(imgl)
    dctx.line(xy)
    del dctx
    return imgp, imgl

def _save_result(imgs, outfilepath):
    # drawing single point on large canvas is not visible
    # for human's eye, so this demo use very small canvas
    # and later resize it.
    size = (imgs[0].width * 8, imgs[0].height * 8)
    imgs[0].resize(size).save(outfilepath + "p.jpg")
    imgs[1].resize(size).save(outfilepath + "l.jpg")

#
xy = []
for x in range(0, 32, 4):
    xy.extend([
            (x,     x // 4 * 2),
            (x + 2, x // 4 * 2),
            (x + 4, x // 4 * 2)])
# calculate bounding box
bb = list(map(int, map(math.ceil, ImagePath.Path(xy).getbbox())))

# with original xy
imgs = _create_demo_imgs(xy, bb[2:])
_save_result(imgs, "result/ImagePath_compact_01_1")

# compact(distance=4)
xy_c = ImagePath.Path(xy)
xy_c.compact(4) #  This method modifies the path in place,
#                  and returns the number of points left
#                  in the path.
#                  distance is measured as Manhattan distance
#                  (https://en.wikipedia.org/wiki/Taxicab_geometry)
#                  and defaults to two pixels.
imgs = _create_demo_imgs(xy_c, bb[2:])
_save_result(imgs, "result/ImagePath_compact_01_2")

# compact(distance=4 + 2)
xy_c = ImagePath.Path(xy)
xy_c.compact(4 + 2)
imgs = _create_demo_imgs(xy_c, bb[2:])
_save_result(imgs, "result/ImagePath_compact_01_3")
