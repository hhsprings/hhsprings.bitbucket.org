from io import BytesIO
import base64
from PIL import Image

img = Image.new("L", (256, 256))
img.putdata([i // 256 for i in range(256*256)])

# You can use a file object instead of a filename.
dst = BytesIO()
# In this case, you must always specify the format
img.save(dst, "JPEG")

# In case of using BytesIO, you can get the written data
# by getvalue() method:
b64ed = base64.standard_b64encode(dst.getvalue())
encoded_image = b"data:image/jpeg;base64," + b64ed

with open("result/encoded_image_01.html", "wb") as fo:
    fo.write(b'<html><body><img src="')
    fo.write(encoded_image)
    fo.write(b'"/></body>\n')
