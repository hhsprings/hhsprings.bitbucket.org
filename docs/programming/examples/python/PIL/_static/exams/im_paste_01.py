from PIL import Image

img1 = Image.open('data/srcimg12.jpg')
img2 = Image.open('data/srcimg13.jpg')
img2 = img2.resize((img2.size[0] // 4, img2.size[1] // 4))

# paste at (0, 0)
res = img1.copy()
res.paste(img2, box=(0, 0, img2.size[0], img2.size[1]))
res.save("result/im_paste_01.jpg")

# paste at (50, 50)
res = img1.copy()
res.paste(img2, box=(50, 50, img2.size[0] + 50, img2.size[1] + 50))
res.save("result/im_paste_02.jpg")

# paste at (50, 50) with mask
res = img1.copy()
mask = Image.new("L", img2.size, 128)
res.paste(
    img2, box=(50, 50, img2.size[0] + 50, img2.size[1] + 50), mask=mask)
res.save("result/im_paste_03.jpg")

# paste at (50, 50) with mask (circle)
res = img1.copy()
mask = Image.open('data/mask_circle_01.jpg')
res.paste(
    img2, box=(50, 50, img2.size[0] + 50, img2.size[1] + 50),
    mask=mask.resize(img2.size))
res.save("result/im_paste_04.jpg")
