from PIL import Image, ImageOps

img = Image.open("data/srcimg23.jpg")  # grayscale ("L")

# 
dimg = ImageOps.colorize(
    img,
    black="midnightblue",
    white="white")
dimg.save("result/ImageOps_colorize_01.jpg")
