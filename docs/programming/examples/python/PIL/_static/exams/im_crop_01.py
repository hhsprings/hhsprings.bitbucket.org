from PIL import Image

img = Image.open('data/srcimg11.png')
bbox = (
    img.size[0] // 4,  # left
    img.size[1] // 4,  # top
    img.size[0] // 4 * 3,  # right
    img.size[1] // 4 * 3   # bottom
    )
img.crop(bbox).save("result/im_crop_01.jpg")
