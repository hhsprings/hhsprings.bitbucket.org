# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont

# get a font
#   This example is for Windows (7, etc.).
#   If you use Unix-like system, fonts are found at
#   for example "/usr/share/fonts".
#fnt = ImageFont.truetype('c:/Windows/Fonts/msmincho.ttc', 30)
fnt = ImageFont.truetype('msmincho.ttc', 30)

img = Image.open("data/srcimg12.jpg")  # open base image

txt = u"東京タワーと三縁山増上寺"  # Tokyo tower and San'en-zan Zōjō-ji
tmpdctx = ImageDraw.Draw(img)  # create drawing context
txtsz = tmpdctx.textsize(txt, fnt)  # calculate text size
del tmpdctx

# create image for blending
osd = Image.new("RGB", (txtsz[0] + 10, txtsz[1] + 10), "skyblue")
dctx = ImageDraw.Draw(osd)  # create drawing context
dctx.text((5, 5), txt, font=fnt, fill="black")  # draw text to osd
del dctx  # destroy drawing context

# blend osd image
img.paste(
    osd,
    box=(20, 420, osd.size[0] + 20, osd.size[1] + 420),
    mask=Image.new("L", osd.size, 192))

img.save("result/ImageDraw_text_03.jpg")
