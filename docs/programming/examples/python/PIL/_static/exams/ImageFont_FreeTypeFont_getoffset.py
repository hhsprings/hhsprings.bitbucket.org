# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont

iw, ih = 500, 120
text = "although"
img = Image.new("RGB", (iw, ih), color="white")
dctx = ImageDraw.Draw(img)

# courbi.ttf: Courier New Bold Italic (on Microsoft Windows)
ttf = ImageFont.truetype("courbi.ttf", 100)
w, h = ttf.getsize(text)
off_x, off_y = ttf.getoffset(text)
mx, my = (iw - w) // 2, (ih - h) // 2

dctx.line(((mx + off_x, my + off_y), (iw - mx, my + off_y)),
          fill="blue")

dctx.rectangle(((mx, my), (iw - mx, ih - my)), outline="red")
dctx.text((mx, my), text, font=ttf, fill="black")
#img.show()
img.save("result/ImageFont_getoffset_01.png")
del dctx
