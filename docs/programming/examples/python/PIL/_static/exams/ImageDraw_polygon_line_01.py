import math
from PIL import Image, ImageDraw
from PIL import ImagePath  # for calculating bounding box

#
ptc = 7
xy = [
    ((math.cos(th) + 1) * 120,
     (math.sin(th) + 1) * 90)
    for th in [i * (2 * math.pi) / ptc for i in range(ptc)]
    ]  # not closed (i.e., end != start)

bbox = ImagePath.Path(xy).getbbox()  # calculate bounding box
size = list(map(int, map(math.ceil, bbox[2:])))

# demo 1 - polygon with outline
img = Image.new("RGB", size, "#f9f9f9")  # create new Image
dctx = ImageDraw.Draw(img)  # create drawing context
dctx.polygon(xy, fill="#eeeeff", outline="blue")  # draw polygon with outline
del dctx  # destroy drawing context
img.save("result/ImageDraw_polygon_line_01.jpg")

# demo 2 - polygon and line with the same xy
img = Image.new("RGB", size, "#f9f9f9")  # create new Image
dctx = ImageDraw.Draw(img)  # create drawing context
dctx.polygon(xy, fill="#eeeeff")  # draw polygon without outline
dctx.line(xy, fill="blue", width=5)  # draw line
del dctx  # destroy drawing context
img.save("result/ImageDraw_polygon_line_02.jpg")
