from PIL import Image

# create RGBA image
img = Image.open('data/srcimg03.jpg')
r, g, b = img.split()
a = Image.new("L", r.size, color=127)
withalpha = Image.merge("RGBA", (r, b, g, a))
withalpha.save("result/im_convert_m_in_01.png")

# convert from RGBA to RGB
withalpha.convert("RGB").save(
    "result/im_convert_m_RGB_01.jpg")
