from itertools import chain
from PIL import Image
#
simg = Image.open('data/srcimg04.jpg')

# --- lookup table by function
#
dimg = simg.point(lambda i: i if i < 220 else 255)
dimg.save("result/im_point_01.jpg")

# --- lookup table by list
#

# all bands share same lookup table
lut = [i if i < 220 else 255
       for i in range(256)] * len(simg.getbands())
dimg = simg.point(lut)
dimg.save("result/im_point_02.jpg")

# each band has individual lookup table
lut = list(chain.from_iterable((
    (i if i < 220 else 255
     for i in range(256)),  # R
    (tuple(range(256))),  # G
    (tuple(range(256))),  # B
    )))

dimg = simg.point(lut)
dimg.save("result/im_point_03.jpg")
