from PIL import Image

simg = Image.open('data/srcimg17.jpg')

(w, h) = simg.size
dimg = simg.transform(
    simg.size,
    method=Image.MESH,
    # data is a list of target rectangles
    # and corresponding source quadrilaterals.
    data=[(
            # target rectangle (1)
            (0, 0, w // 2, h // 2),
            # corresponding source quadrilateral (1)
            # (NW, SW, SE, and NE. see method=QUAD)
            (0, 0, 0, h, w, h, w - 100, 0)
           ),
          (
            # target rectangle (2)
            (w // 2, h // 2, w, h),
            # corresponding source quadrilateral (2)
            # (NW, SW, SE, and NE. see method=QUAD)
            (0, 0, 0, h, w, h, w - 100, 0)
            ),
          ]
    )
dimg.save(
    "result/im_transform_MESH_01.jpg")
