from PIL import Image, ImageOps

img = Image.open('data/srcimg11.png')
ImageOps.crop(img, img.size[1] // 4).save(
    "result/ImageOps_crop_01.jpg")
