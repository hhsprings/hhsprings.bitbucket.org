from PIL import Image, ImageOps

#
for bn in ("srcimg20", "srcimg21", "srcimg22", "srcimg23",):
    img = Image.open("data/" + bn + ".jpg")
    for i, cutoff in enumerate((
            0,
            0.2,  # 20%
            0.4   # 40%
            )):
        dimg = ImageOps.autocontrast(img, cutoff=cutoff)
        dimg.save(
            "result/ImageOps_autocontrast_%s_0%d.jpg" % (bn, i + 1))
