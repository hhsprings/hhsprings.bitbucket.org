from PIL import Image, ImageChops

# It seems two images must be same size for ImageChops.blend,
# but documents doesn't mention that.
img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg").resize(img1.size)

dimg = ImageChops.blend(img1, img2, alpha=0.25)
dimg.save("result/ImageChops_blend_01_a025.jpg")
dimg = ImageChops.blend(img1, img2, alpha=0.5)
dimg.save("result/ImageChops_blend_01_a050.jpg")
dimg = ImageChops.blend(img1, img2, alpha=0.75)
dimg.save("result/ImageChops_blend_01_a075.jpg")
