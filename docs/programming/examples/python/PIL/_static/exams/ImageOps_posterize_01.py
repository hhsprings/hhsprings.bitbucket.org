from PIL import Image, ImageOps

simg = Image.open('data/srcimg06.jpg')
#
for i in (1, 2, 3, 4, 5, 6, 7, 8):
    dimg = ImageOps.posterize(simg, bits=i)
    dimg.save("result/ImageOps_posterize_b%d.jpg" % i)
