from PIL import Image, ImageEnhance

img = Image.open('data/srcimg22.jpg')
#
enhancer = ImageEnhance.Brightness(img)
enhancer.enhance(0.0).save(
    "result/ImageEnhance_Brightness_000.jpg")
enhancer.enhance(0.25).save(
    "result/ImageEnhance_Brightness_025.jpg")
enhancer.enhance(0.5).save(
    "result/ImageEnhance_Brightness_050.jpg")
enhancer.enhance(0.75).save(
    "result/ImageEnhance_Brightness_075.jpg")
enhancer.enhance(1.0).save(
    "result/ImageEnhance_Brightness_100.jpg")
