from PIL import Image
#
img1 = Image.open('data/srcimg10.png')
r, b, g = img1.split()
a = Image.new("L", r.size, "white")
img1 = Image.merge("RGBA", (r, b, g, a))

# img2 has alpha
img2 = Image.open('data/srcimg09.png')
img2 = img2.resize(img1.size)

#
Image.alpha_composite(img1, img2).save(
    "result/Image_alpha_composite_01.png")
