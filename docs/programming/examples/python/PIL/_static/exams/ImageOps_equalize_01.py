from PIL import Image, ImageOps
#
fn = 'data/srcimg07.jpg'
simg = Image.open(fn)
simg_gs = simg.convert("L")  # grayscale
simg_gs.save("result/ImageOps_equalize_sgs.jpg")

# Equalize the image histogram.
dimg = ImageOps.equalize(simg)
dimg.save("result/ImageOps_equalize_d.jpg")
dimg_gs = dimg.convert("L")  # grayscale
dimg_gs.save("result/ImageOps_equalize_dgs.jpg")

#
# visualize histogram
import matplotlib.pyplot as plt  # https://matplotlib.org
bins = list(range(256))
plt.plot(bins, simg_gs.histogram(), 'r')
plt.plot(bins, dimg_gs.histogram(), 'g')
plt.xlabel('Pixel value')
plt.ylabel('Frequency')
plt.title(fn)
plt.legend(
    ('src (grayscale)', 'dest (grayscale)'),
     shadow=True, loc=(0.01, 0.75))
plt.grid(True)
plt.savefig("result/ImageOps_equalize_hist_01.jpg")
