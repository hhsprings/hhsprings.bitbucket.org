from PIL import Image
#
img1 = Image.open('data/srcimg01.jpg')
img2 = Image.open('data/srcimg02.jpg').resize(img1.size)
mask = Image.open('data/mask_grad_01.jpg')
mask = mask.resize(img1.size)
#
Image.composite(img1, img2, mask).save(
    "result/Image_composite_03.jpg")
