from PIL import Image

img = Image.open('data/srcimg04.jpg')
img = img.convert(
    "P",
    dither=Image.FLOYDSTEINBERG,  # default
    palette=Image.WEB  # default
    #, colors=216  # standard 216-color "web palette"
    )
img.save(
    "result/im_convert_P_01.png")
