from PIL import Image
#
simg = Image.open('data/srcimg14.jpg')
# angle, resample, expand
#   resample can be one of Image.NEAREST (default),
#   Image.BILINEAR, Image.BICUBIC
dimg = simg.rotate(-10)
dimg.save("result/im_rotate_r-10.jpg")
#
dimg = simg.rotate(-10, expand=True)
dimg.save("result/im_rotate_r-10_expand.jpg")

