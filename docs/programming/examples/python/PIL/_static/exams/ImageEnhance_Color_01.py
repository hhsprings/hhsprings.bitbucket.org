from PIL import Image, ImageEnhance

img = Image.open('data/srcimg06.jpg')
#
enhancer = ImageEnhance.Color(img)
enhancer.enhance(0.0).save(
    "result/ImageEnhance_Color_000.jpg")
enhancer.enhance(0.25).save(
    "result/ImageEnhance_Color_025.jpg")
enhancer.enhance(0.5).save(
    "result/ImageEnhance_Color_050.jpg")
enhancer.enhance(0.75).save(
    "result/ImageEnhance_Color_075.jpg")
enhancer.enhance(1.0).save(
    "result/ImageEnhance_Color_100.jpg")
