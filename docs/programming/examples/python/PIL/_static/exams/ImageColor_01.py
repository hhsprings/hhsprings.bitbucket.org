import colorsys
from PIL import ImageColor

colors = [
    (n, c,
     ImageColor.getcolor(c, "L"),  # as gray scale
     colorsys.rgb_to_hls(
            *map(lambda _: _ / 255., ImageColor.getrgb(c))  # as (r, g, b) floats
             )  # => HLS
     )
    for n, c in ImageColor.colormap.items()]

s2c = sorted(colors, key=lambda _: _[1])  # order by (r, g, b)
s3c = sorted(colors, key=lambda _: _[2])  # order by grayscale value
s4c = sorted(colors, key=lambda _: _[3])  # order by (hue, lum, sat)
s5c = sorted(colors, key=lambda _: (_[3][2], _[3][1], _[3][0]))  # order by (sat, lum, hue)

print("<html><body>")
print("""
<table border=1>
<thead>
<tr>
<th colspan=2>order by (r, g, b)</th>
<th colspan=2>order by grayscale</th>
<th colspan=2>order by (hue, lum, sat)</th>
<th colspan=2>order by (sat, lum, hue)</th>
</tr>
</thead>
""")
for c2, c3, c4, c5 in zip(s2c, s3c, s4c, s5c):
    print("""\
<tr>
<td>{}</td><td width='50pt' style='background-color: {}'>&nbsp;</td>
<td>{}</td><td width='50pt' style='background-color: {}'>&nbsp;</td>
<td>{}</td><td width='50pt' style='background-color: {}'>&nbsp;</td>
<td>{}</td><td width='50pt' style='background-color: {}'>&nbsp;</td>
</tr>
""".format(c2[0], c2[1], c3[0], c3[1], c4[0], c4[1], c5[0], c5[1]))
print("</table>")
print("</body></html>")
