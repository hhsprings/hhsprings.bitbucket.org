from PIL import Image, ImageOps

img = Image.open('data/srcimg05.jpg')
# left, top = right, bottom = (100, 50)
dimg = ImageOps.expand(img, border=(100, 50))
dimg.save("result/ImageOps_expand_03.jpg")

# left, top, right, bottom = (20, 30, 40, 50)
dimg = ImageOps.expand(img, border=(20, 30, 40, 50))
dimg.save("result/ImageOps_expand_04.jpg")
