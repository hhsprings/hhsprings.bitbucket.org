# collaborate with putalpha
from PIL import Image
#
simg = Image.open('data/srcimg14.jpg')
dimg = simg.rotate(-10)
mask = Image.new("L", simg.size, 255)
mask = mask.rotate(-10)
dimg.putalpha(mask)
dimg.save("result/im_rotate_r-10m.png")
#
dimg = simg.rotate(-10, expand=True)
mask = Image.new("L", simg.size, 255)
mask = mask.rotate(-10, expand=True)
dimg.putalpha(mask)
dimg.save("result/im_rotate_r-10m_expand.png")

