from PIL import Image
from PIL import ImageFilter
from PIL.ImageFilter import (
    UnsharpMask
    )
simg = Image.open('data/srcimg07.jpg')

# defaut: radius=2, percent=150, threshold=3
dimg = simg.filter(UnsharpMask(radius=2, percent=150, threshold=3))
dimg.save("result/ImageFilter_UnsharpMask_2_150_3.jpg")
