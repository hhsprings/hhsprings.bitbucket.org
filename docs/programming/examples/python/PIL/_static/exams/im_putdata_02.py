# from pure python list data
from PIL import Image

img = Image.new("RGB", (64, 64))  # multiple bands
gr_r = [0] * 64 * 64
gr_g = list(range(0, 256, 4)) * 64
gr_b = list(range(0, 256, 4)) * 64
img.putdata(list(zip(gr_r, gr_g, gr_b)))
img.save(
    "result/im_putdata_02.jpg")
