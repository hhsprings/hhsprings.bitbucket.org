from PIL import Image
from PIL import ImageFilter
img = Image.open('data/srcimg05.jpg')

# one kind of emboss
km = (
     -2, -1,  0,
     -1,  1,  1,
      0,  1,  2
      )
k = ImageFilter.Kernel(
    size=(3, 3),
    kernel=km,
    scale=sum(km),  # default
    offset=0  # default
    )
img.filter(k).save(
    "result/ImageFilter_Kernel_01.jpg")
