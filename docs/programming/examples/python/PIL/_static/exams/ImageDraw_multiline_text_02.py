# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
from PIL import ImageFilter

# get a font
fnt = ImageFont.truetype('msmincho.ttc', 300)

img = Image.open("data/srcimg12.jpg")  # open base image

# text to draw
txt = u"""東京タワーと
三縁山増上寺"""  # Tokyo tower and San'en-zan Zōjō-ji

# calculate text size
dctx = ImageDraw.Draw(img)  # create drawing context (of img)
txtsz = dctx.multiline_textsize(txt, fnt)
del dctx

# draw text to mask
mask = Image.new("L", (txtsz[0], txtsz[1]), 64)
dctx = ImageDraw.Draw(mask)  # create drawing context (of mask)
dctx.multiline_text(
    (0, 0),
    txt,
    font=fnt,
    fill=255  # full opacity
    )

del dctx  # destroy drawing context

# add some effect
for i in range(10):
    mask = mask.filter(ImageFilter.BLUR)

# putmask to base image
img.putalpha(mask.resize(img.size))

img.save("result/ImageDraw_multiline_text_02.png")
