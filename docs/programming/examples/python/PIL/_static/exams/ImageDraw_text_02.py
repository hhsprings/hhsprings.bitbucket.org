# -*- coding: utf-8 -*-
# simple banner.
#
from __future__ import unicode_literals
from PIL import Image, ImageDraw, ImageFont

fnt = ImageFont.truetype('CENTURY.TTF', 64)
txt = "Readability counts."
img = Image.new("L", (1, 1))  # temp to calculate size
draw = ImageDraw.Draw(img)
img = img.resize(draw.textsize(txt, fnt))  # re-create
draw = ImageDraw.Draw(img)
draw.text((0, 0), txt, font=fnt, fill=255)
img = img.rotate(-90, expand=True)
# values varies 0 to 255 => 0 or 1 => mapping to char => join
_TAB = ["  ", "##"]
_NLT = ["\n", ""]
print("".join([
            (_TAB[v > 0] + _NLT[bool((i + 1) % img.width)])
            for i, v in enumerate(img.getdata())]))
