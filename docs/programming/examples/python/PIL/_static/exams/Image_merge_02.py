import numpy as np
from PIL import Image
#
img = Image.open('data/srcimg08.jpg')
r, g, b = img.split()

# build alpha image (with numpy)
ap = np.array(img.convert("L").getdata()).reshape(img.size)
ap[ap >= 128] = 255
ap[ap < 128] = 0
a = Image.new("L", img.size)
a.putdata(ap.flatten())

#
Image.merge("RGBA", (b, g, r, a)).save(
    "data/srcimg09.png")
