from PIL import Image
#
img = Image.open('data/srcimg24.jpg')
red, grn, blu = img.split()
red.save("result/im_split_01_rL.jpg")
grn.save("result/im_split_01_gL.jpg")
blu.save("result/im_split_01_bL.jpg")
blk = Image.new("L", red.size, "black")

Image.merge("RGB", (red, blk, blk)).save(
    "result/im_split_01_r.jpg")
Image.merge("RGB", (blk, grn, blk)).save(
    "result/im_split_01_g.jpg")
Image.merge("RGB", (blk, blk, blu)).save(
    "result/im_split_01_b.jpg")
