from PIL import Image

img = Image.open('data/srcimg04.jpg')
# When palette is ADAPTIVE, the parameter dither makes no sense (ignored).
img = img.convert(
    "P",
    palette=Image.ADAPTIVE,  # an optimized palette
    colors=32
    )
img.save(
    "result/im_convert_P_03.png")
