from PIL import Image, ImageOps

simg = Image.open("data/srcimg12.jpg")

dimg = ImageOps.solarize(simg, threshold=128)  # default
dimg.save("result/ImageOps_solarize_01.jpg")

dimg = ImageOps.solarize(simg, threshold=50)
dimg.save("result/ImageOps_solarize_02.jpg")

dimg = ImageOps.solarize(simg, threshold=0)  # identical to ImageOps.invert
dimg.save("result/ImageOps_solarize_03.jpg")
