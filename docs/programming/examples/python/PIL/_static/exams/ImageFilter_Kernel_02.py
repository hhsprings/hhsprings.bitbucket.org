from PIL import Image
from PIL import ImageFilter
import numpy as np

img = Image.open('data/srcimg05.jpg')

# Gaussian blur 5 x 5 (approximation)
km = np.array((
        (1, 4, 6, 4, 1),
        (4, 16, 24, 16, 4),
        (6, 24, 36, 24, 6),
        (4, 16, 24, 16, 4),
        (1, 4, 6, 4, 1),
      )) / 256.
k = ImageFilter.Kernel(
    size=km.shape,
    kernel=km.flatten(),
    scale=np.sum(km),  # default
    offset=0  # default
    )
img.filter(k).save(
    "result/ImageFilter_Kernel_02.jpg")
