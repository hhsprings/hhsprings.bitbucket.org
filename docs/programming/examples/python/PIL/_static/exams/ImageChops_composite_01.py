from PIL import Image, ImageChops

# It seems two images and mask must be same size for ImageChops.composite,
# but documents doesn't mention that.
img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg").resize(img1.size)

#
mask = Image.open("data/mask_circle_01.jpg").resize(img1.size)  # circle
#
dimg = ImageChops.composite(img1, img2, mask)
dimg.save("result/ImageChops_composite_01.jpg")
