import os
from PIL import Image, ImageMorph
from PIL.ImageMorph import LutBuilder, MorphOp

for bn in (
    "binary_shapes_01",
    "binary_noisy_letters_01",
    "binary_noisy_letters_02",
    ):

    img = Image.open(os.path.join("data", bn + ".jpg"))
    img.load()  # MorphOp.apply expects image are loaded.

    for op_name in (
        'corner',
        'dilation4', 'dilation8',
        'erosion4', 'erosion8',
        'edge',
        ):
        lb = LutBuilder(op_name=op_name)
        mop = MorphOp(lb.build_lut())
        _, dimg = mop.apply(img)
        dimg.save(
            os.path.join(
                "result",
                "ImageMorph01_" + bn + "_" + op_name + ".jpg"))
