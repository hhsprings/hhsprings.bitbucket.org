from PIL import Image, ImageOps

img = Image.open('data/srcimg05.jpg')
dimg = ImageOps.expand(img, border=100)
dimg.save("result/ImageOps_expand_01.jpg")
dimg = ImageOps.expand(img, border=100, fill="burlywood")
dimg.save("result/ImageOps_expand_02.jpg")
