#
# PIL histogram to graph with matplotlib (https://matplotlib.org)
#
from PIL import Image
import matplotlib.pyplot as plt
#
fn = 'data/srcimg03.jpg'
simg = Image.open(fn)
r, g, b = simg.split()

bins = list(range(256))
plt.plot(bins, r.histogram(), 'r')
plt.plot(bins, g.histogram(), 'g')
plt.plot(bins, b.histogram(), 'b')
plt.xlabel('Pixel value')
plt.ylabel('Frequency')
plt.title(fn)
plt.grid(True)
plt.savefig("result/im_histogram_01.jpg")
