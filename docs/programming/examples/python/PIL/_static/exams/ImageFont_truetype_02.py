# -*- coding: utf-8 -*-
# This example is for Windows.
#
# This example draws a character using the Microsoft Symbol font,
# which uses the "symb" encoding and characters
# in the range 0xF000 to 0xF0FF.
#
from __future__ import unicode_literals
from textwrap import TextWrapper
from PIL import Image, ImageDraw, ImageFont
try:
    unichr(0)
except:
    unichr = chr
txtwrap = TextWrapper(width=32).fill

font = ImageFont.truetype("symbol.ttf", 35, encoding="symb")
txt = txtwrap("".join([unichr(0xF000 + i) for i in range(0x100)]))
img = Image.new("L", (900, 360), 255)
draw = ImageDraw.Draw(img)
draw.multiline_text((0, 0), txt, font=font)
#img.show()
img.save("result/ImageFont_symbol_01.png")
del draw
