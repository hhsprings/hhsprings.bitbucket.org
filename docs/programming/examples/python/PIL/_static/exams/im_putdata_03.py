# from numpy array
import numpy as np
from PIL import Image

img = Image.new("L", (64, 64))  # single band
graddata = np.ndarray((64, 64))
for i in range(64):
    graddata[:, i] = i * 4
img.putdata(graddata.flatten())
img.save(
    "result/im_putdata_03.jpg")
