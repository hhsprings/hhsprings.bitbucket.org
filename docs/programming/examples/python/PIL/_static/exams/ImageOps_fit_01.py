from PIL import Image, ImageOps

simg = Image.open('data/srcimg11.png')
#
dimg = ImageOps.fit(simg,
    size=(simg.size[0], simg.size[1] // 3),
    centering=(0.5, 0.0))  # 50%, 0%
dimg.save("result/ImageOps_fit_01.jpg")
#
dimg = ImageOps.fit(simg,
    size=(simg.size[0], simg.size[1] // 3),
    centering=(0.5, 0.5))  # 50%, 50% <= default
dimg.save("result/ImageOps_fit_02.jpg")
#
dimg = ImageOps.fit(simg,
    size=(simg.size[0], simg.size[1] // 3),
    centering=(0.5, 1.0))  # 50%, 100%
dimg.save("result/ImageOps_fit_03.jpg")
#
dimg = ImageOps.fit(simg,
    size=(simg.size[0] // 3, simg.size[1]),
    centering=(0.0, 0.5))  # 0%, 50%
dimg.save("result/ImageOps_fit_04.jpg")

#
dimg = ImageOps.fit(simg,
    size=(simg.size[0] // 3, simg.size[1]),
    centering=(0.5, 0.5))  # 50%, 50% <= default
dimg.save("result/ImageOps_fit_05.jpg")
#
dimg = ImageOps.fit(simg,
    size=(simg.size[0] // 3, simg.size[1]),
    centering=(1.0, 0.5))  # 100%, 50%
dimg.save("result/ImageOps_fit_06.jpg")
