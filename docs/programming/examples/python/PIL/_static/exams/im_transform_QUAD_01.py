# quadrilateral warp.  data specifies the four corners
# given as NW, SW, SE, and NE.
from itertools import chain
from PIL import Image

simg = Image.open('data/srcimg17.jpg')

_nw = (0, 190)
_sw = (140, simg.height - 150)
_se = (simg.width - 160, simg.height - 70)
_ne = (simg.width, 0)
dimg = simg.transform(
    (simg.width, simg.height),
    method=Image.QUAD,
    data=list(chain.from_iterable((_nw, _sw, _se, _ne))))
dimg.save(
    "result/im_transform_QUAD_01.jpg")
