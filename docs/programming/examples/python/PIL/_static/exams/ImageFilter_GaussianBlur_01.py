from PIL import Image
from PIL import ImageFilter
from PIL.ImageFilter import (
    GaussianBlur
    )
simg = Image.open('data/srcimg05.jpg')

# defaut: radius=2
dimg = simg.filter(GaussianBlur(radius=10))
dimg.save("result/ImageFilter_GaussianBlur_10.jpg")
