from PIL import Image, ImageChops

# It seems modes of two images must be "1" for ImageChops.logical_or,
# but documents doesn't mention that...
img1 = Image.open("data/srcimg18.jpg").convert("1")
img2 = Image.open("data/srcimg19.jpg").convert("1")

dimg = ImageChops.logical_or(img1, img2)
dimg.save("result/ImageChops_logical_or_01.jpg")
