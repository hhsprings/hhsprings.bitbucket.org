from PIL import Image, ImageChops

img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg")

dimg = ImageChops.add(img1, img2, scale=1.0)
dimg.save("result/ImageChops_add_01_s1.jpg")
dimg = ImageChops.add(img1, img2, scale=2.0)
dimg.save("result/ImageChops_add_01_s2.jpg")
