from PIL import Image, ImageDraw, ImageChops

dia = 128
circle = Image.new("L", (dia * 4, dia * 4), 0)
dctx = ImageDraw.Draw(circle)
dctx.ellipse([dia, dia, dia * 3, dia * 3], fill=255)
del dctx

offset = dia // 2
r = ImageChops.offset(circle, offset, offset)
g = ImageChops.offset(circle, -offset, offset)
b = ImageChops.offset(circle, 0, -offset)

dimg = Image.merge("RGB", (r, g, b))
mask = Image.eval(dimg.convert("L"), lambda p: 255 if p > 0 else 0)
dimg.putalpha(mask)
dimg.save("result/Image_merge_02.png")
