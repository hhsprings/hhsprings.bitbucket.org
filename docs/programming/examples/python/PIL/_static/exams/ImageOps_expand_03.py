from PIL import Image, ImageOps

def _resize_to_square(img, fill=0):
    if img.width > img.height:
        border = (0, (img.width - img.height) // 2)
    elif img.width < img.height:
        border = ((img.height - img.width) // 2, 0)
    else:
        return img.copy()
    return ImageOps.expand(img, border, fill)

img = Image.open('data/srcimg05.jpg')
dimg = _resize_to_square(img)
dimg.save("result/ImageOps_expand_05.jpg")
dimg = _resize_to_square(img.rotate(90, expand=True))
dimg.save("result/ImageOps_expand_06.jpg")
