#! /bin/env python
# -*- coding: utf-8 -*-
# epscalwi is a wrapper to pscal (http://www.panix.com/~mbh/projects.html).
#
# This script is a part of
# `Pillow (PIL) examples <https://hhsprings.bitbucket.io/docs/programming/examples/python/PIL/>`_,
# so the main purpose of this script is a simple demonstration of Pillow (PSDraw, and EPSImagePlugin).
# Don't complain its incompleteness :)
#
#
from __future__ import unicode_literals

import sys
import subprocess
from io import BytesIO
from PIL import Image

VERSION = '0.2'
# from ver 0.1 to 0.2: accept no photo image.


def _get_boundingbox(psdoc):
    import os, subprocess, tempfile
    from PIL import EpsImagePlugin

    t_fd, tfile = tempfile.mkstemp()
    os.close(t_fd)
    open(tfile, "w").write(psdoc)

    gs_binary = EpsImagePlugin.gs_windows_binary
    gs_binary = ("gs" if not gs_binary else gs_binary)
    command = [gs_binary, "-dBATCH", "-dNOPAUSE", "-sDEVICE=bbox", "-q", tfile]
    gs_output = subprocess.check_output(
        command, stderr=subprocess.STDOUT)
    return gs_output.decode("us-ascii").strip().split("\n")


def _call_pscal(args):
    arglist = ["-d", args.d, "-L", "English"]
    if args.M:
        arglist.append("-M")
    if args.m:
        arglist.append("-m")
    if args.n:
        arglist.append("-n")
    if args.s:
        arglist.append("-s")
    else:
        arglist.append("-S")
    if len(args.month_year):
        arglist.extend(args.month_year[:2])

    if sys.platform == 'win32':
        # please install Unix-like environment like MSYS, or cygwin,
        # and place pscal and this script in the same directory.
        command = ["sh", "pscal"] + list(map(str, arglist))
    else:
        command = ["pscal"] + list(map(str, arglist))
    return subprocess.check_output(command)


def _build_ps(args, outfp=sys.stdout):
    from PIL import PSDraw, ImageOps

    resample = Image.BICUBIC
    #
    no_photo = (args.photo_image == ":devnull:")

    # A4 portrait: 8.27 x 11.69 inches
    #           => 595, 834 pixels if dpi=72
    dpi_fac = 1  # for understanding dpi, so this has no meaning for me.

    # load the output of pscal to PIL.Image.
    # NOTE: Because original image from pscal is scalable, so it's beautiful,
    #   But once it is loaded as raster, it will be dirty...
    #   Ideally, eps from pscal should be inserted directly, maybe.
    #   But we can try to do render a little better, with scale option
    #   of load, and resample option of various transform methods.
    pscalimg = Image.open(BytesIO(_call_pscal(args)))
    pscalimg.load(scale=12)  # for hi-res (warn: large scale needs much memory)
    pscalimg = pscalimg.rotate(-90, resample=resample, expand=True)
    pscalimg = pscalimg.resize(
        (834 * dpi_fac, 1190 // 2 * dpi_fac), resample=resample)
    #
    psd = PSDraw.PSDraw(outfp)
    psd.begin_document()
    # outer box (to avoid crop by ghostscript)
    height = 1190 if not no_photo else 1190 // 2
    psd.line((0, 0), (834, 0))
    psd.line((834, 0), (834, height))
    psd.line((834, height), (0, height))
    psd.line((0, height), (0, 0))

    # photo to top, pscal to bottom
    mx, my = 20, 1
    psd.image(
        (mx, my, (834 - mx), (1190 // 2 - my)),
        pscalimg, dpi=72 * dpi_fac)
    if not no_photo:
        photoimg = Image.open(args.photo_image)
        photoimg = photoimg.resize(
            (834 * dpi_fac, 1190 // 2 * dpi_fac), resample=resample)
        psd.image((mx, 1190 // 2 + my, 834 - mx, 1190 - my),
                  photoimg, dpi=72 * dpi_fac)

    psd.end_document()


def main(args):
    import os

    dn = os.path.dirname(args.out_file)
    fn = os.path.basename(args.out_file)
    bn, ext = os.path.splitext(fn)

    #
    out = BytesIO()
    _build_ps(args, out)
    result = out.getvalue().decode("us-ascii")
    #
    if ext != ".ps":
        # append BB to second line
        bb = _get_boundingbox(result)  # get BB from GS
        s1, _, s2 = result.partition("\n")
        result = s1 + _ + bb[0] + _ + s2
    #
    if ext in (".ps", ".eps"):
        open(fn, "w").write(result)
    else:
        cnv = Image.open(BytesIO(result.encode("us-ascii")))
        cnv.save(fn)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", type=int, default=13, help="moon diameter (default = 13)")
    parser.add_argument(
        "-M", action="store_true", help="show moon phases (southern hemisphere)")
    parser.add_argument(
        "-m", action="store_true", help="show moon phases (northern hemisphere)")
    parser.add_argument(
        "-n", action="store_true", help="show day numbering")
    parser.add_argument(
        "-S", action="store_false", help="European style (Monday first)", dest="s")
    parser.add_argument(
        "-s", action="store_true", help="American style (Sunday first)")
    parser.add_argument(
        "photo_image", help="""\
photo image (if you don't want this, specify :devnull:)""")  # args for epscalwi
    parser.add_argument(
        "out_file", help="output file name")  # args for epscalwi
    parser.add_argument(
        "month_year", type=int, nargs="*", help="'month year' or 'month'")
    #
    main(parser.parse_args())
