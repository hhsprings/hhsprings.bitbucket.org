# from numpy array
import numpy as np
from PIL import Image

img = Image.new("RGB", (64, 64))  # multiple bands
graddata = np.zeros((3, 64, 64), dtype=np.int32)
for i in range(64):
    graddata[1:, :, i] = i * 4
img.putdata(
    list(
        zip(graddata[0].flatten(),
            graddata[1].flatten(),
            graddata[2].flatten())))
img.save(
    "result/im_putdata_04.jpg")
