from PIL import Image, ImageChops

img = Image.open("data/srcimg11.png")

dimg = ImageChops.offset(
    img,
    xoffset=img.size[0] // 3,
    yoffset=img.size[1] // 3,
    )
dimg.save("result/ImageChops_offset_01.jpg")
