from PIL import Image, ImageChops

img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg")

dimg = ImageChops.subtract(img1, img2, scale=1.0)
dimg.save("result/ImageChops_subtract_01_s1.jpg")
dimg = ImageChops.subtract(img1, img2, scale=0.5)
dimg.save("result/ImageChops_subtract_01_s2.jpg")
