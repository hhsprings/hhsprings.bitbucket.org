import numpy as np
from PIL import Image

img = Image.open('data/srcimg12.jpg')
r = np.array(img.getdata(0))
g = np.array(img.getdata(1))
b = np.array(img.getdata(2))
a = np.ones(r.shape) * 255
a[np.logical_and(r < 50, r < 50, g < 50)] = 0
alpha = Image.new("L", img.size)
alpha.putdata(a.flatten())

# alpha cab be an "L" or "1" image having the same size as this image
img.putalpha(alpha)
img.save(
    "result/im_putalpha_02.png")
