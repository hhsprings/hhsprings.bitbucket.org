from PIL import Image
from PIL import ImageFilter
from PIL.ImageFilter import (
    RankFilter, MedianFilter, MinFilter, MaxFilter
    )
simg = Image.open('data/srcimg06.jpg')

#
dimg = simg.filter(RankFilter(size=9, rank=2))
dimg.save("result/ImageFilter_RankFilter_9_2.jpg")
#
dimg = simg.filter(MinFilter(size=9))
dimg.save("result/ImageFilter_MinFilter_9.jpg")
#
dimg = simg.filter(MedianFilter(size=9))
dimg.save("result/ImageFilter_MedianFilter_9.jpg")
#
dimg = simg.filter(MaxFilter(size=9))
dimg.save("result/ImageFilter_MaxFilter_9.jpg")
