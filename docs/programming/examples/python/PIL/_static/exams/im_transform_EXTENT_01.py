from PIL import Image

simg = Image.open('data/srcimg16.jpg')

#
x0, y0 = 10, 0
x1, y1 = 10 + simg.width // 4, simg.height // 3
dimg = simg.transform(
    simg.size,  # expand to original size
    method=Image.EXTENT,
    data=[x0, y0, x1, y1])
dimg.save(
    "result/im_transform_EXTENT_01.jpg")

#
x0, y0 = 10, 0
x1, y1 = 10 + simg.width // 4, simg.height // 3
dimg = simg.transform(
    (x1 - x0, y1 - y0),  # to cropped size
    method=Image.EXTENT,
    data=[x0, y0, x1, y1])
dimg.save(
    "result/im_transform_EXTENT_02.jpg")
