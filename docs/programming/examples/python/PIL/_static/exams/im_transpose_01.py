from PIL import Image
#
img = Image.open('data/srcimg14.jpg')
img.transpose(Image.FLIP_LEFT_RIGHT).save(
    "result/im_transpose_FLIP_LEFT_RIGHT.jpg")
img.transpose(Image.FLIP_TOP_BOTTOM).save(
    "result/im_transpose_FLIP_TOP_BOTTOM.jpg")
img.transpose(Image.ROTATE_90).save(
    "result/im_transpose_ROTATE_90.jpg")
img.transpose(Image.ROTATE_180).save(
    "result/im_transpose_ROTATE_180.jpg")
img.transpose(Image.ROTATE_270).save(
    "result/im_transpose_ROTATE_270.jpg")
img.transpose(Image.TRANSPOSE).save(
    "result/im_transpose_TRANSPOSE.jpg")
