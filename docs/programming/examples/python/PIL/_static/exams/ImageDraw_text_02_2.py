# -*- coding: utf-8 -*-
# simple "text to xpm" converter
#
from __future__ import unicode_literals

import sys
from PIL import Image, ImageDraw, ImageFont


if __name__ == '__main__':
    fnt = ImageFont.truetype('cour.ttf', 24)
    txt = sys.argv[1]
    name = sys.argv[1].lower()  # FIXME: more safely
    img = Image.new("L", (1, 1))  # temp to calculate size
    draw = ImageDraw.Draw(img)
    img = img.resize(draw.textsize(txt, fnt))  # re-create
    draw = ImageDraw.Draw(img)
    draw.text((0, 0), txt, font=fnt, fill=255)
    # values varies 0 to 255 => 0 to 2 => mapping to char => join
    _PAL = [(".", "#ffffff"), ("o", "#7f7f7f"), ("#", "#000000")]
    _NLT = ["\n", ""]
    palette = "\n".join(['"{} c {}",'.format(ch, cl) for ch, cl in _PAL])
    databody = "".join([
        (_PAL[int(v / 127)][0] + _NLT[bool((i + 1) % img.width)])
        for i, v in enumerate(img.getdata())]).strip().split("\n")
    databody = "\n".join(['"{}",'.format(d) for d in databody])
    print("""\
/* XPM */
static char * {}_xpm[] = {{
"{:d} {:d} {:d} {:d}",
{}
{}
}};
""".format(name, img.width, img.height, len(_PAL), 1, palette, databody))
