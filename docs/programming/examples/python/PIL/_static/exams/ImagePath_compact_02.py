import numpy as np
from PIL import Image, ImagePath, ImageDraw

# epicycloid
rc, rm = 5, 1
th = np.linspace(0, 2 * np.pi, 360)
X = (rc + rm) * np.cos(th) - rm * np.cos(((rc + rm) / rm) * th)
Y = (rc + rm) * np.sin(th) - rm * np.sin(((rc + rm) / rm) * th)
X = (X - X.min()) * 30
Y = (Y - Y.min()) * 30

#
xy = list(zip(X, Y))
ipath = ImagePath.Path(xy)
bb = list(map(int, map(np.ceil, ipath.getbbox())))

# original path
img = Image.new("RGB", bb[2:], color="#f0f0ff")
dctx = ImageDraw.Draw(img)
dctx.line(ipath, fill="blue")
del dctx
img.save("result/ImagePath_compact_02_0.jpg")

# compact (repeat)
for i in range(5):
    img = Image.new("RGB", bb[2:], color="#f0f0ff")
    dctx = ImageDraw.Draw(img)
    ipath.compact((i + 1) * 15)  # modifies the path in place
    dctx.line(ipath, fill="blue")
    del dctx
    img.save("result/ImagePath_compact_02_%d.jpg" % i)
