from PIL import Image, ImageDraw
from PIL import ImageColor

img = Image.open("data/srcimg07.jpg")  # load base image
dctx = ImageDraw.Draw(img)  # create drawing context
bmsz = (img.width // 16 - 10, img.height // 16 - 10)
#
# NOTE: ImageColor.colormap is undocumented attribute.
colors = list(ImageColor.colormap.keys())
for y in range(16):
    for x in range(16):
        bm = Image.new("L", bmsz)
        dctx_inner = ImageDraw.Draw(bm)
        dctx_inner.ellipse(
            [(0, 0), bm.size],
            fill=y * 16 + x  # (y * 16 + x) varies in range(0, 256)
            )
        del dctx_inner

        pos = [
            ((bmsz[0] + 10) * x + 10,
             (bmsz[1] + 10) * y + 10)]
        dctx.bitmap(
            pos,
            # pixel values of bm is used as mask to fill.
            bm,
            fill=colors[(y * 16 + x) % len(colors)])
#
del dctx  # destroy drawing context
img.save("result/ImageDraw_bitmap_01.png")
