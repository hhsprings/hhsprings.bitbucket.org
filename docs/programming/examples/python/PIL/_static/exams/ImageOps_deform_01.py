from PIL import Image, ImageOps

class _Deformer(object):

    # The deformer should provide a getmesh method,
    # which returns a MESH list suitable for the Image
    # transform method. See the transform method for details.
    def getmesh(self, img):

        (w, h) = img.size

        # data is a list of target rectangles
        # and corresponding source quadrilaterals.
        return [(
                # target rectangle (1)
                (0, 0, w // 2, h // 2),
                # corresponding source quadrilateral (1)
                # (NW, SW, SE, and NE. see method=QUAD)
                (0, 0, 0, h, w, h, w - 100, 0)
               ),
              (
                # target rectangle (2)
                (w // 2, h // 2, w, h),
                # corresponding source quadrilateral (2)
                # (NW, SW, SE, and NE. see method=QUAD)
                (0, 0, 0, h, w, h, w - 100, 0)
                ),
              ]

#
simg = Image.open('data/srcimg17.jpg')
dimg = ImageOps.deform(simg, _Deformer())
dimg.save(
    "result/ImageOps_deform_01.jpg")
