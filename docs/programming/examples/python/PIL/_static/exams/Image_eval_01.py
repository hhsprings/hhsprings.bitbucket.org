from PIL import Image

#
def func1(p):
    return max(0, p - 50)
#
def func2(p):
    return 255 - p

#
img = Image.open('data/srcimg01.jpg')
Image.eval(img, func1).save(
    "result/Image_eval_01.jpg")
Image.eval(img, func2).save(
    "result/Image_eval_02.jpg")
