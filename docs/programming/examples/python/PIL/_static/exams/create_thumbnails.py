# -*- coding: utf-8 -*-
#
# very simple script for creating thumbnails.
#
import argparse
import os
from glob import iglob
from PIL import Image

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_dir", default=".")
    parser.add_argument("-d", "--dest_dir", default=".")
    parser.add_argument("-s", "--size", type=int, default=128)
    parser.add_argument(
        "-e", "--extentension",
        choices=["thumbnail", "jpg", "jpeg"],
        default="jpg")
    args = parser.parse_args()
    size = (args.size, args.size)

    for pat in ("*.jpg", "*.jpeg", "*.png"):
        for fpath in iglob(os.path.join(args.input_dir, pat)):
            fn = os.path.basename(fpath)
            bn, ext = os.path.splitext(fn)
            #
            img = Image.open(fpath)

            # NOTE:
            #  * Aspect ratio is preserved.
            #  * This function modifies Image object in place.
            #  * The parameter 'resample' differs between PIL
            #    (or pillow) versions.
            img.thumbnail(size)
            #

            if not os.path.exists(args.dest_dir):
                os.mkdir(args.dest_dir)
            img.save(
                os.path.join(
                    args.dest_dir,
                    "{}.{}".format(bn, args.extentension)),
                "JPEG")
