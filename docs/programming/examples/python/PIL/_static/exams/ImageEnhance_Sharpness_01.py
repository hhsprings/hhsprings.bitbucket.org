from PIL import Image, ImageEnhance

img = Image.open('data/srcimg05.jpg')
#
enhancer = ImageEnhance.Sharpness(img)
enhancer.enhance(0.0).save(
    "result/ImageEnhance_Sharpness_000.jpg")
enhancer.enhance(0.5).save(
    "result/ImageEnhance_Sharpness_050.jpg")
enhancer.enhance(1.0).save(
    "result/ImageEnhance_Sharpness_100.jpg")
enhancer.enhance(1.5).save(
    "result/ImageEnhance_Sharpness_150.jpg")
enhancer.enhance(2.0).save(
    "result/ImageEnhance_Sharpness_200.jpg")
