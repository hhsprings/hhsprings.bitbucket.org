import math
from PIL import Image, ImagePath, ImageDraw

xy = [
    (0, 0),
    # box with diagonal
    ( 50, 100),  # left, top
    (160, 100),  # right, top
    (160, 150),  # right, bottom
    ( 50, 150),  # left, bottom
    ( 50, 100),  # left, top
    (160, 150),  # right, bottom
    ]

#
ipath = ImagePath.Path(xy)
bb = (0, 0, 210, 160)

# original path
img1 = Image.new("RGB", bb[2:], color="#f0f0ff")
dctx = ImageDraw.Draw(img1)
dctx.line(ipath, fill="blue")
del dctx
img1.save("result/ImagePath_transform_01_0.jpg")

# affine transform
theta = math.pi / 15.
ipath.transform((
        math.cos(theta), math.sin(theta), 20,
        -math.sin(theta), math.cos(theta), 20,
        ))
img2 = Image.new("RGB", bb[2:], color="#f0f0ff")
dctx = ImageDraw.Draw(img2)
dctx.line(ipath, fill="blue")
del dctx
img2.save("result/ImagePath_transform_01_1.jpg")
