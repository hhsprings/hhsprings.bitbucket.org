# from pure python list data
from PIL import Image

img = Image.new("L", (64, 64))  # single band
graddata = list(range(0, 256, 4)) * 64
img.putdata(graddata)
img.save(
    "result/im_putdata_01.jpg")
