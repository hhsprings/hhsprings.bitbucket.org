import math
from PIL import Image

simg = Image.open('data/srcimg15.jpg')

#
# shear
#
cx, cy = 0.1, 0
dimg = simg.transform(
    simg.size,
    method=Image.AFFINE,
    data=[1, cx, 0,
          cy, 1, 0,])
dimg.save(
    "result/im_transform_AFFINE__shear1.jpg")
#
cx, cy = 0, 0.1
dimg = simg.transform(
    simg.size,
    method=Image.AFFINE,
    data=[1, cx, 0,
          cy, 1, 0,])
dimg.save(
    "result/im_transform_AFFINE__shear2.jpg")

#
# rotate
#
theta = math.radians(-5)
dimg = simg.transform(
    simg.size,
    method=Image.AFFINE,
    data=[math.cos(theta), math.sin(theta), 0,
          -math.sin(theta), math.cos(theta), 0,])
dimg.save(
    "result/im_transform_AFFINE__rotate.jpg")
