from PIL import Image

img = Image.open('data/srcimg03.jpg')
# rgb2xyz
mat = (
    0.412453, 0.357580, 0.180423, 0,
    0.212671, 0.715160, 0.072169, 0,
    0.019334, 0.119193, 0.950227, 0 )
#
img.convert("RGB", mat).save(
    "result/im_convert_mm_01.jpg")
