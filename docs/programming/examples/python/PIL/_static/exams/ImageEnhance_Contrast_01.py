from PIL import Image, ImageEnhance

img = Image.open('data/srcimg21.jpg')
#
enhancer = ImageEnhance.Contrast(img)
enhancer.enhance(0.0).save(
    "result/ImageEnhance_Contrast_000.jpg")
enhancer.enhance(0.25).save(
    "result/ImageEnhance_Contrast_025.jpg")
enhancer.enhance(0.5).save(
    "result/ImageEnhance_Contrast_050.jpg")
enhancer.enhance(0.75).save(
    "result/ImageEnhance_Contrast_075.jpg")
enhancer.enhance(1.0).save(
    "result/ImageEnhance_Contrast_100.jpg")
