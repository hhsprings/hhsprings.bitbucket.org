# This example is for Windows.
from PIL import Image, ImageDraw, ImageFont

pangram = "Pack my box with five dozen liquor jugs."

fonts = [
    ImageFont.truetype('gulim', 28, index=i)
    for i in range(4)]
texts = [
    str(font.getname()) + ": " + pangram
    for font in fonts
    ]
#
size = [0, 0]
for s in [fonts[i].getsize(txt) for txt in texts]:
    size = [max(size[0], s[0]), size[1] + s[1]]
#
img = Image.new("L", size, 255)
draw = ImageDraw.Draw(img)
y = 0
for font, txt in zip(fonts, texts):
    draw.text((0, y), txt, font=font, fill=0)
    y += font.getsize(txt)[1]
#img.show()
img.save("result/ImageFont_gulim_01.png")
del draw
