# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont

# get a font
#   This example is for Windows (7, etc.).
#   If you use Unix-like system, fonts are found at
#   for example "/usr/share/fonts".
#fnt = ImageFont.truetype('c:/Windows/Fonts/msmincho.ttc', 30)
fnt = ImageFont.truetype('msmincho.ttc', 30)

img = Image.open("data/srcimg12.jpg")  # open base image
dctx = ImageDraw.Draw(img)  # create drawing context

# multiline text to draw
txt = u"""\
東京タワーと三縁山増上寺
(Tokyo tower and San'en-zan Zōjō-ji)"""

# calculate text size
spacing = 2
txtsz = dctx.multiline_textsize(txt, fnt, spacing=spacing)

# draw text
dctx.multiline_text(
    # draw text at near (left, bottom)
    (20, img.height - txtsz[1] - 20),
    txt,
    font=fnt,
    fill="#eeeeff",
    spacing=spacing,
    align="center"
    )

del dctx  # destroy drawing context

img.save("result/ImageDraw_multiline_text_01.jpg")
