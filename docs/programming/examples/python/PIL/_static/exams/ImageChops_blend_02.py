#
# NOTE: This demo works with pillow version >= 3.4.
#
from PIL import Image, ImageChops

# It seems two images must be same size for ImageChops.blend,
# but documents doesn't mention that.
img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg").resize(img1.size)

# save as animation gif
# see:
#   https://pillow.readthedocs.io/en/latest/handbook/image-file-formats.html#saving
# NOTE: file size of animation GIF grows very huge!
frames = [
    ImageChops.blend(
        img1, img2, alpha=1.0 / 32 * i).resize(
        (img1.width // 4, img1.height // 4))
    for i in range(32 + 1)
    ]
frames[0].save(
    "result/ImageChops_blend_02.gif",
    save_all=True,
    append_images=frames[1:],
    optimize=True,
    duration=100,  # [ms]
    loop=255)
