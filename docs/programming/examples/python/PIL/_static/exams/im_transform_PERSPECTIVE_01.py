import numpy as np
from numpy import linalg
from PIL import Image

def _create_coeff(
    xyA1, xyA2, xyA3, xyA4,
    xyB1, xyB2, xyB3, xyB4):

    A = np.array([
            [xyA1[0], xyA1[1], 1, 0, 0, 0, -xyB1[0] * xyA1[0], -xyB1[0] * xyA1[1]],
            [0, 0, 0, xyA1[0], xyA1[1], 1, -xyB1[1] * xyA1[0], -xyB1[1] * xyA1[1]],
            [xyA2[0], xyA2[1], 1, 0, 0, 0, -xyB2[0] * xyA2[0], -xyB2[0] * xyA2[1]],
            [0, 0, 0, xyA2[0], xyA2[1], 1, -xyB2[1] * xyA2[0], -xyB2[1] * xyA2[1]],
            [xyA3[0], xyA3[1], 1, 0, 0, 0, -xyB3[0] * xyA3[0], -xyB3[0] * xyA3[1]],
            [0, 0, 0, xyA3[0], xyA3[1], 1, -xyB3[1] * xyA3[0], -xyB3[1] * xyA3[1]],
            [xyA4[0], xyA4[1], 1, 0, 0, 0, -xyB4[0] * xyA4[0], -xyB4[0] * xyA4[1]],
            [0, 0, 0, xyA4[0], xyA4[1], 1, -xyB4[1] * xyA4[0], -xyB4[1] * xyA4[1]],
            ], dtype=np.float32)
    B = np.array([
            xyB1[0],
            xyB1[1],
            xyB2[0],
            xyB2[1],
            xyB3[0],
            xyB3[1],
            xyB4[0],
            xyB4[1],
            ], dtype=np.float32)
    return linalg.solve(A, B)

#
simg = Image.open('data/srcimg17.jpg')
coeff = _create_coeff(
    (0, 0),
    (simg.width, 0),
    (simg.width, simg.height),
    (0, simg.height),
    # =>
    (-500, 0),
    (simg.width + 500, 0),
    (simg.width, simg.height),
    (0, simg.height),
    )
dimg = simg.transform(
    (simg.width, simg.height),
    method=Image.PERSPECTIVE,
    data=coeff)
dimg.save(
    "result/im_transform_PERSPECTIVE_01.jpg")

