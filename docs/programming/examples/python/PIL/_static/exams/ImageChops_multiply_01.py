from PIL import Image, ImageChops

img1 = Image.open("data/srcimg18.jpg")
img2 = Image.open("data/srcimg19.jpg")

dimg = ImageChops.multiply(img1, img2)
dimg.save("result/ImageChops_multiply_01.jpg")
