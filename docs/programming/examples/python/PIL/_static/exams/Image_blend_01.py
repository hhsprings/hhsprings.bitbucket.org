import sys
from PIL import Image
#
alpha = float(sys.argv[1])
img1 = Image.open('data/srcimg01.jpg')
img2 = Image.open('data/srcimg02.jpg').resize(img1.size)
#
Image.blend(img1, img2, alpha).save(
    "result/Image_blend_{}.jpg".format(alpha))
