from PIL import Image
import numpy as np

# lower-triangle
data = np.tril(np.ones((256, 256)) * 255)
mask = Image.new("L", (256, 256))
mask.putdata(data.flatten().tolist())
mask.save("mask_tril_01.jpg")
