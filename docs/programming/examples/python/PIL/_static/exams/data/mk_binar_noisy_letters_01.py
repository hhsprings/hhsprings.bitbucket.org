import numpy as np
from PIL import Image, ImageDraw, ImageFont, ImageOps

fnt = ImageFont.truetype('CENTURY.TTF', 26)
txt = """\
Pack my box with five dozen liquor jugs.
Jackdaws love my big sphinx of quartz.
The five boxing wizards jump quickly.
How vexingly quick daft zebras jump!
Bright vixens jump; dozy fowl quack.
Sphinx of black quartz, judge my vow.
""" * 2

img = Image.new("L", (512, 512), 0)
draw = ImageDraw.Draw(img)
txtsize = draw.multiline_textsize(txt, fnt, spacing=10)
for i in range(3):
    draw.multiline_text(
        (img.width // 2 + i - txtsize[0] // 2,
         img.height // 2 + i - txtsize[1] // 2),
        txt, font=fnt, align="center", spacing=10, fill=255)
img.save("binary_noisy_letters_01.jpg")
xya = np.random.random_sample((512 * 128, 2)) * 512
for i, (x, y) in enumerate(xya):
    draw.point((x, y), fill=i % 256)
img.save("binary_noisy_letters_02.jpg")

del draw
