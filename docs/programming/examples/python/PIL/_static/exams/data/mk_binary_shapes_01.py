from PIL import Image, ImageDraw

mask = Image.new("L", (512, 512), 255)
draw = ImageDraw.Draw(mask)
for i in range(2, -1, -1):
    draw.rectangle([15 - i, 15 - i, 301 + i, 301 + i], fill=255, outline=0)
draw.rectangle([20, 20, 120, 120], fill=0, outline=0)
draw.ellipse([50, 50, 400, 400], fill=0, outline=0)
draw.ellipse([280, 90, 330, 140], fill=255, outline=0)
draw.rectangle([90, 90, 260, 260], fill=255, outline=0)
draw.ellipse([150, 150, 500, 500], fill=255, outline=0)
draw.ellipse([350, 350, 400, 400], fill=0, outline=0)
del draw
mask.save("binary_shapes_01.jpg")
