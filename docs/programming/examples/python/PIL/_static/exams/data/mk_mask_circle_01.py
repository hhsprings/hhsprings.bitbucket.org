from PIL import Image, ImageDraw

mask = Image.new("L", (256, 256), "black")
draw = ImageDraw.Draw(mask)
draw.ellipse(
    [0, 0, 256, 256], fill=255)
del draw
mask.save("mask_circle_01.jpg")
