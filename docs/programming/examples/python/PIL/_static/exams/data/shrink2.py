import sys
from PIL import Image

n = sys.argv[1]
img = Image.open(n)
img = img.resize((img.size[0] // 2, img.size[1] // 2))
extsize = img.size[0] // 2
dimg = Image.new("RGB", (img.size[0] + extsize, img.size[1] + extsize), "#333333")
dimg.paste(img, box=(extsize//2, extsize//2, img.size[0] + extsize//2, img.size[1] + extsize//2))
dimg.save(n)
