from PIL import Image
import numpy as np

data = np.ndarray((256, 256))
for i in range(256):
    data[i,:] = i
mask = Image.new("L", (256, 256))
mask.putdata(data.flatten().tolist())
mask.save("mask_grad_01.jpg")
