import sys
from PIL import Image

n = sys.argv[1]
img = Image.open(n)
img = img.resize((img.size[0] // 2, img.size[1] // 2))
img.save(n)

