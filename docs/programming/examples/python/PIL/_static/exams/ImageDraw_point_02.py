import math
from PIL import Image, ImageDraw
from PIL import ImagePath  # for calculating bounding box

#
xy = [
    (i, (math.sin(i / 64. * math.pi) + 1) * 128)
    for i in range(256)
    ]

bbox = ImagePath.Path(xy).getbbox()  # calculate bounding box
size = list(map(int, map(math.ceil, bbox[2:])))

img = Image.new("RGB", size, "#f9f9f9")  # create new Image
dctx = ImageDraw.Draw(img)  # create drawing context
dctx.point(xy, fill="blue")  # draw points
del dctx  # destroy drawing context
img.save("result/ImageDraw_point_02.jpg")
