from PIL import Image, ImageOps

img1 = Image.open("data/srcimg18.jpg")

# ImageOps.invert is maybe identical to ImageChops.invert.
dimg = ImageOps.invert(img1)
dimg.save("result/ImageOps_invert_01.jpg")
