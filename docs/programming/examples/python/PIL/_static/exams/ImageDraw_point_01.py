from PIL import Image, ImageDraw

# drawing single point on large canvas is not visible for human's eye,
# so this demo use very small canvas and later resize it.

img = Image.new("RGB", (16, 16), "#f9f9f9")  # create new Image
dctx = ImageDraw.Draw(img)  # create drawing context
dctx.point([(2, 3)], fill="blue")  # draw points
dctx.point([(5, 8)], fill="red")  # draw points
del dctx  # destroy drawing context
img.resize((128, 128)).save("result/ImageDraw_point_01.jpg")
